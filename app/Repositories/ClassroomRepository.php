<?php

namespace App\Repositories;

use App\Http\Requests\ClassroomRequest;
use App\Models\Classroom;
interface ClassroomRepository{

    public function store(ClassroomRequest $request);

    public function find(int $id);

    public function findOrFail(int $id);

    public function findByCode(string $code);

    public function destroy(Int $id);

    public function update(Classroom $classroom, ClassroomRequest $request);
}
