<?php

namespace App\Repositories;



use App\Models\User;

class EloquentUserRepository implements UserRepository
{
    public function find(int $id)
    {
        return User::find($id);
    }

    public function findOrFail(int $id)
    {
        return User::findOrFail($id);
    }
}
