<?php

namespace App\Repositories;

use App\Http\Requests\SchoolsRequest;
use App\Models\School;
class EloquentSchoolRepository implements SchoolRepository
{
    public function store(SchoolsRequest $request)
    {

        return School::create([
            'inep' => $request->inep,
            'name' => $request->name,
            'uf' => $request->uf,
            'city' => $request->city,
        ]);
    }

    public function find(int $id)
    {
        return School::find($id);
    }

    public function findOrFail(int $id)
    {
        return School::findOrFail($id);
    }
}
