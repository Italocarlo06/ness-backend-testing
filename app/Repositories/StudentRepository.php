<?php


namespace App\Repositories;

use App\Http\Requests\StudentRequest;
use App\Models\Student;

interface StudentRepository
{
    public function store(StudentRequest $request);

    public function find(int $id);

    public function findOrFail(int $id);

    public function destroy(Int $studentId);

    public function update(Student $student, StudentRequest $request);
}
