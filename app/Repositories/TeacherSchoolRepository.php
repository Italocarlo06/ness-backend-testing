<?php


namespace App\Repositories;


interface TeacherSchoolRepository
{
    public function findAllSchoolsByTeacher(Int $teacherId);
}

