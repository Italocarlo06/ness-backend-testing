<?php

namespace App\Repositories;

use App\Models\TeacherSchool;

class EloquentTeacherSchoolRepository implements TeacherSchoolRepository
{

    public function findAllSchoolsByTeacher(int $teacherId)
    {
        $schools = TeacherSchool::where('user_id', $teacherId)->get();
        return $schools;
    }
}
