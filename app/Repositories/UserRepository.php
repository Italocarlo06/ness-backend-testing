<?php
namespace App\Repositories;
interface  UserRepository {
    public function find(int $id);

    public function findOrFail(int $id);
}
