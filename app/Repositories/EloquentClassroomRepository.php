<?php

namespace App\Repositories;

use App\Http\Requests\ClassroomRequest;
use App\Models\Classroom;
use App\Models\ClassroomStudent;
use Illuminate\Support\Facades\DB;

class EloquentClassroomRepository implements ClassroomRepository{

    public function store(ClassroomRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $classroom = Classroom::create([
                'school_id' => $request->escola,
                'teacher_id' => $request->responsavel,
                'code' => $request->codigo
            ]);
            $classroomStudents = [];
            foreach ($request->alunos as $studentId) {
                $classroomStudents[] = [
                    'classroom_id' => $classroom->id,
                    'student_id' => $studentId,
                    'created_at' => now(),
                    'updated_at' => now(),

                ];
            }
            ClassroomStudent::insert($classroomStudents);

            return $classroom;
        });
    }

    public function find(int $id)
    {
        return Classroom::find($id);
    }

    public function findOrFail(int $id)
    {
        return Classroom::findOrFail($id);
    }

    public function destroy(int $id)
    {
        return Classroom::destroy($id);
    }

    public function update(Classroom $classroom, ClassroomRequest $request)
    {
        return DB::transaction(function () use ($request,$classroom ) {
            if ($request->has('escola')) {
                $classroom->school_id = $request->escola;
            }
            if ($request->has('responsavel')) {
                $classroom->teacher_id = $request->responsavel;
            }
            $classroom->update();
            if ($request->has('alunos')) {
                $classroom->students()->sync($request->alunos, ['created_at' => now()]);

            }

            return $classroom;
        });
    }

    public function findByCode(string $code)
    {
        $classroom = Classroom::where("code",$code)->get()->first();
        return $classroom;
    }
}
