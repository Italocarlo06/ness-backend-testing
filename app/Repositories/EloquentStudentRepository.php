<?php

namespace App\Repositories;

use App\Http\Requests\SchoolsRequest;
use App\Http\Requests\StudentRequest;
use App\Models\Student;

class EloquentStudentRepository implements StudentRepository
{
    public function store(StudentRequest $request)
    {
        $student = Student::create([
            'name' => $request->name,
            'responsible_name' => '13246',
            'birth_date' => $request->birth_date,
            'school_id' => $request->school_id,
        ]);
        return $student;
    }

    public function find(int $id)
    {
        return Student::find($id);
    }

    public function findOrFail(int $id)
    {
        return Student::findOrFail($id);
    }

    public function update(Student $student, StudentRequest $request){
        if ($request->has('name')) {
            $student->name = $request->name;
        }
        if ($request->has('responsible_name')) {
            $student->responsible_name = $request->responsible_name;
        }
        if ($request->has('school_id')) {
            $student->school_id = $request->school_id;
        }
        if ($request->has('birth_date')) {
            $student->birth_date = $request->birth_date;
        }
            $student->save();
            return $student;
    }

    public function destroy(int $studentId)
    {
        Student::destroy($studentId);
    }
}
