<?php

namespace App\Repositories;

use App\Http\Requests\SchoolsRequest;

interface SchoolRepository {
    public function store(SchoolsRequest $request);
    public function find(Int $id);
    public function findOrFail(Int $id);
}
