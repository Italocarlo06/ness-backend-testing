<?php

namespace App\Services;

use App\Http\Requests\SchoolsRequest;
use App\Repositories\SchoolRepository;

class SchoolService {

    public function __construct(
        protected SchoolRepository $repository) {
    }

    public function store(SchoolsRequest $request)
    {
        return $this->repository->store($request);
    }

    public function find(Int $id)
    {
        return $this->repository->find($id);
    }
}
