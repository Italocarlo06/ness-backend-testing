<?php


namespace App\Services;

use App\Http\Requests\StudentRequest;
use App\Repositories\SchoolRepository;
use App\Repositories\StudentRepository;

class StudentService
{

    public function __construct(
        protected StudentRepository $repository,
        protected SchoolRepository $schoolRepository)
    {
    }

    public function store(StudentRequest $request)
    {
        $school = $this->schoolRepository->findOrFail($request->school_id);
        return $this->repository->store($request);
    }

    public function update(StudentRequest $request, Int $studentId)
    {
        $student = $this->repository->findOrFail($studentId);

        if($request->has('school_id')){
            $school = $this->schoolRepository->findOrFail($request->school_id);
        }

        return $this->repository->update($student, $request);
    }

    public function find(Int $studentId)
    {
        return $this->repository->find($studentId);
    }

    public function destroy(Int $studentId)
    {
        $school = $this->repository->findOrFail($studentId);
        return $this->repository->destroy($studentId);
    }
}
