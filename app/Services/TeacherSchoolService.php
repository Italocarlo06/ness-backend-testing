<?php

namespace App\Services;

use App\Repositories\TeacherSchoolRepository;

class TeacherSchoolService {

    public function __construct(
        protected TeacherSchoolRepository $repository) {
    }

    public function findAllSchoolsByTeacher(Int $teacherId)
    {
        $schoolsTeacher = $this->repository->findAllSchoolsByTeacher($teacherId);
        $schoolsList = $schoolsTeacher ->map(function ($schoolTeacher) {
            return $schoolTeacher->school;
        });
        return $schoolsList;
    }
}
