<?php

namespace App\Services;
use App\Http\Requests\ClassroomRequest;
use App\Repositories\ClassroomRepository;
use App\Repositories\SchoolRepository;
use App\Repositories\StudentRepository;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ClassroomService {

    public function __construct(
        protected UserRepository $userRepository,
        protected SchoolRepository $schoolRepository,
        protected StudentRepository $studentRepository,
        protected ClassroomRepository $repository) {
    }
    public function store(ClassroomRequest $request)
    {
        $existsClassroom = $this->repository->findByCode($request->codigo);
        if ($existsClassroom){
            throw new BadRequestException("Code is already registered");
        }
        $this->schoolRepository->findOrFail($request->escola);
        $this->userRepository->findOrFail($request->responsavel);
        foreach ($request->alunos as $student){
            $this->studentRepository->findOrFail($student);
        }

        $classroom = $this->repository->store($request);

        return $classroom;
    }

    public function update(ClassroomRequest $request, Int $id)
    {
        $classroom = $this->repository->findOrFail($id);

        if ($request->has('escola')){
            $this->schoolRepository->findOrFail($request->escola);
        }
        if ($request->has('responsavel')){
            $this->userRepository->findOrFail($request->responsavel);
        }
        if ($request->has('alunos')){
            foreach ($request->alunos as $student){
                $this->studentRepository->findOrFail($student);
            }
        }

        $updatedClassRoom = $this->repository->update($classroom,$request);


        return $updatedClassRoom;
    }

    public function find(Int $id)
    {
        return $this->repository->find($id);
    }

    public function destroy(Int $id)
    {
        $this->repository->findOrFail($id);
        return $this->repository->destroy($id);
    }

    public function findStudentsByClassroom (Int $id)
    {
        $classroom = $this->repository->findOrFail($id);
        return $classroom->students;
    }
}
