<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use App\Services\StudentService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class StudentController extends Controller
{
    public function __construct(
        protected StudentService $studentService) {
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(StudentRequest $request)
    {
        try {
            $student = $this->studentService->store($request);
            return $student;
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => 'School not found!',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StudentRequest $request, Int $studentId)
    {
        try {
            $student = $this->studentService->update($request, $studentId);
            return $student;
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => 'School or Student not found!',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        }
    }

    /**
     * Find the specified resource in storage.
     */
    public function find(Int $studentId)
    {

        $student = $this->studentService->find($studentId);
        if (!$student){
            return response()->json([
                'message' => 'Student not found!',
            ], Response::HTTP_NOT_FOUND);
        }
        return $student;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Int $studentId)
    {
        try {
            $student = $this->studentService->destroy($studentId);
            return response()->json($student, Response::HTTP_NO_CONTENT);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Student not found!',
            ], Response::HTTP_NOT_FOUND);

        }

    }
}
