<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClassroomRequest;
use App\Services\ClassroomService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ClassroomController extends Controller
{
    public function __construct(
        protected ClassroomService $classroomService) {
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(ClassroomRequest $request)
    {
        try {
            $classroom = $this->classroomService->store($request);
            return $classroom;
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => 'School or Teacher or Students not found!',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        }catch(BadRequestException $e) {
            return response()->json([
                'message' => 'Code is already registered',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        }
    }

    /**
     * Find the specified resource in storage.
     */
    public function find(Int $id)
    {

        $student = $this->classroomService->find($id);
        if (!$student){
            return response()->json([
                'message' => 'Classroom not found!',
            ], Response::HTTP_NOT_FOUND);
        }
        return $student;
    }



    /**
     * Update the specified resource in storage.
     */
    public function update(ClassroomRequest $request, Int $id)
    {
        try {
            $classroom = $this->classroomService->update($request, $id);
            return $classroom;
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => 'School or Students or Teacher not found!',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Int $id)
    {
        try {
            $this->classroomService->destroy($id);
            return response()->json([],Response::HTTP_NO_CONTENT);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Classroom not found!',
            ], Response::HTTP_NOT_FOUND);

        }
    }

    public function findStudentsByClassroom(Int $id)
    {
        try {
            $students = $this->classroomService->findStudentsByClassroom($id);
            return response()->json($students,Response::HTTP_OK);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Classroom not found!',
            ], Response::HTTP_NOT_FOUND);

        }
    }
}
