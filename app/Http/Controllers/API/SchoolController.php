<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSchoolRequest;
use App\Http\Requests\UpdateSchoolRequest;
use App\Services\TeacherSchoolService;
use App\Models\School;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class SchoolController extends Controller
{
    public function __construct(
        protected TeacherSchoolService $teacherSchoolService) {
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $userId = Auth::id();
        $schools = $this->teacherSchoolService->findAllSchoolsByTeacher($userId);
        if ($schools->isEmpty()){
            return response()->json([],Response::HTTP_NO_CONTENT);
        }

        return  $schools;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSchoolRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(School $school)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSchoolRequest $request, School $school)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(School $school)
    {
        //
    }
}
