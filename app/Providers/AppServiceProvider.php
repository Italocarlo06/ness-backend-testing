<?php

namespace App\Providers;

use App\Repositories\ClassroomRepository;
use App\Repositories\EloquentClassroomRepository;
use App\Repositories\EloquentSchoolRepository;
use App\Repositories\EloquentStudentRepository;
use App\Repositories\EloquentTeacherSchoolRepository;
use App\Repositories\EloquentUserRepository;
use App\Repositories\SchoolRepository;
use App\Repositories\StudentRepository;
use App\Repositories\TeacherSchoolRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(UserRepository::class, function () {
            return new EloquentUserRepository();
        });

        $this->app->singleton(SchoolRepository::class, function () {
            return new EloquentSchoolRepository();
        });

        $this->app->singleton(StudentRepository::class, function () {
            return new EloquentStudentRepository();
        });

        $this->app->singleton(ClassroomRepository::class, function () {
            return new EloquentClassroomRepository();
        });

        $this->app->singleton(TeacherSchoolRepository::class, function () {
            return new EloquentTeacherSchoolRepository();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
