<?php

use App\Http\Controllers\API\ClassroomController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\SchoolController;
use App\Http\Controllers\API\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function(){
    Route::get('escolas', [SchoolController::class, 'index']);

    Route::prefix('alunos')->group(function () {
        Route::post('/', [StudentController::class, 'store']);
        Route::put('/{id}', [StudentController::class, 'update']);
        Route::get('/{id}', [StudentController::class, 'find']);
        Route::delete('/{id}', [StudentController::class, 'destroy']);
    });

    Route::prefix('turmas')->group(function () {
        Route::post('/', [ClassroomController::class, 'store']);
        Route::put('/{id}', [ClassroomController::class, 'update']);
        Route::get('/{id}', [ClassroomController::class, 'find']);
        Route::get('/{id}/alunos', [ClassroomController::class, 'findStudentsByClassroom']);
        Route::delete('/{id}', [ClassroomController::class, 'destroy']);
    });

});
