<?php

namespace Tests\Feature;

use App\Models\Student;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;
    public function testCreateStudent()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->make();

        $response = $this->post('/api/alunos',$student->toArray());
        $response->assertStatus(201);
    }

    public function testCreateStudentFailedWithWrongSchoolId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->make([
            "school_id" => 500
        ]);

        $response = $this->post('/api/alunos',$student->toArray());
        $response->assertStatus(422);
    }


    public function testGetStudentById()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create();

        $response = $this->get('/api/alunos/'.$student->id);
        $response->assertStatus(200);
    }

    public function testGetStudentFailedByIdWithWrongId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create();

        $response = $this->get('/api/alunos/500');
        $response->assertStatus(404);
    }

    public function testDeleteStudent()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create();

        $response = $this->delete('/api/alunos/'.$student->id);
        $response->assertStatus(204);

    }

    public function testDeleteStudentFailedByIdWithWrongId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create();

        $response = $this->delete('/api/alunos/500');
        $response->assertStatus(404);
    }

    public function testUpdateStudent()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create();
        $newStudentData = Student::factory()->make([
            "name" => "New Student Name",
            "responsible_name" => "New Student Responsible Name"
        ]);
        $response = $this->put('/api/alunos/'.$student->id,$newStudentData->toArray());
        $response->assertStatus(200);
    }

    public function testUpdateStudentFailedWithWrongSchoolId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create();
        $newStudentData = Student::factory()->make([
            "name" => "New Student Name",
            "school_id" => 500
        ]);
        $response = $this->put('/api/alunos/'.$student->id,$newStudentData->toArray());
        $response->assertStatus(422);
    }

    public function testUpdateStudentFailedWithWrongId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $newStudentData = Student::factory()->make([
            "name" => "New Student Name",
            "school_id" => 500
        ]);
        $response = $this->put('/api/alunos/500',$newStudentData->toArray());
        $response->assertStatus(422);
    }
}
