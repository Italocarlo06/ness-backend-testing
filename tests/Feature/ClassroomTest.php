<?php

namespace Tests\Feature;

use App\Models\Classroom;
use App\Models\School;
use App\Models\Student;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ClassroomTest extends TestCase
{
    use RefreshDatabase;
    public function testCreateClassRoom()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $students = Student::factory(3)->create();


        $classroom = Classroom::factory()->make();

        $classroomData = [
            "responsavel" => $classroom->teacher_id,
            "escola" => $classroom->school_id,
            "codigo" => $classroom->code,
            "alunos" => $students->pluck('id')->toArray()
        ];

        $response = $this->post('/api/turmas',$classroomData);
        $response->assertStatus(201);
    }

    public function testCreateClassroomFailedWithCodeAlreadyRegistered()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $students = Student::factory(3)->create();


        $code = '123456';
        Classroom::factory()->create([
            'code' => $code
        ]);

        $classroom = Classroom::factory()->make([
            'code' => $code
        ]);


        $classroomData = [
            "responsavel" => $classroom->teacher_id,
            "escola" => $classroom->school_id,
            "codigo" => $classroom->code,
            "alunos" => $students->pluck('id')->toArray()
        ];

        $response = $this->post('/api/turmas',$classroomData);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'Code is already registered',
        ]);

    }

    public function testCreateClassroomFailedWithWrongTeacherId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $students = Student::factory(3)->create();




        $classroom = Classroom::factory()->make([
            'teacher_id' => 3000
        ]);

        $classroomData = [
            "responsavel" => $classroom->teacher_id,
            "escola" => $classroom->school_id,
            "codigo" => $classroom->code,
            "alunos" => $students->pluck('id')->toArray()
        ];

        $response = $this->post('/api/turmas',$classroomData);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'School or Teacher or Students not found!',
        ]);

    }

    public function testCreateClassroomFailedWithWrongSchoolId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $students = Student::factory(3)->create();


        $classroom = Classroom::factory()->make([
            'school_id' => 3000
        ]);

        $classroomData = [
            "responsavel" => $classroom->teacher_id,
            "escola" => $classroom->school_id,
            "codigo" => $classroom->code,
            "alunos" => $students->pluck('id')->toArray()
        ];

        $response = $this->post('/api/turmas',$classroomData);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'School or Teacher or Students not found!',
        ]);

    }

    public function testCreateClassroomFailedWithWrongStudentId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->make([
            'school_id' => 3000
        ]);

        $classroomData = [
            "responsavel" => $classroom->teacher_id,
            "escola" => $classroom->school_id,
            "codigo" => $classroom->code,
            "alunos" =>[20,30,40]
        ];

        $response = $this->post('/api/turmas',$classroomData);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'School or Teacher or Students not found!',
        ]);

    }


    public function testGetClassroomById()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();

        $response = $this->get('/api/turmas/'.$classroom->id);
        $response->assertStatus(200);
    }

    public function testGetClassroomFailedByIdWithWrongId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/api/turmas/500');
        $response->assertStatus(404);
    }

    public function testDeleteClassroom()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();

        $response = $this->delete('/api/turmas/'.$classroom->id);
        $response->assertStatus(204);

    }

    public function testDeleteClassroomFailedByIdWithWrongId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->delete('/api/turmas/500');
        $response->assertStatus(404);
    }

    public function testUpdateClassroomNewTeacherId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();

        $teacher = User::factory()->create();

        $updateclassroomData = [
            "responsavel" => $teacher->id,
        ];

        $response = $this->put('/api/turmas/'.$classroom->id,$updateclassroomData);
        $response->assertStatus(200);

        $this->assertDatabaseHas('classrooms', [
            'id' => $classroom->id,
            'code' => $classroom->code,
            'teacher_id' => $teacher->id,
            'school_id' => $classroom->school_id,
        ]);

    }


    public function testUpdateClassroomNewSchoolId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();

        $school = School::factory()->create();

        $updateclassroomData = [
            "escola" => $school->id,
        ];

        $response = $this->put('/api/turmas/'.$classroom->id,$updateclassroomData);
        $response->assertStatus(200);

        $this->assertDatabaseHas('classrooms', [
            'id' => $classroom->id,
            'code' => $classroom->code,
            'teacher_id' => $classroom->teacher_id,
            'school_id' => $school->id,
        ]);

    }

    public function testUpdateClassroomNewStudents()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();

        $students = Student::factory(3)->create();

        $updateclassroomData = [
            "alunos" => $students->pluck('id')->toArray()
        ];

        $response = $this->put('/api/turmas/'.$classroom->id,$updateclassroomData);
        $response->assertStatus(200);

        foreach ($students as $student){
            $this->assertDatabaseHas('classrooms_students', [
                'classroom_id' => $classroom->id,
                'student_id' => $student->id,
            ]);
        }


    }

    public function testUpdateClassroomWithWrongTeacherId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();


        $updateclassroomData = [
            "responsavel" => 123456,
        ];

        $response = $this->put('/api/turmas/'.$classroom->id,$updateclassroomData);
        $response->assertStatus(422);

    }

    public function testUpdateClassroomWithWrongSchoolId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();


        $updateclassroomData = [
            "escola" => 123456,
        ];

        $response = $this->put('/api/turmas/'.$classroom->id,$updateclassroomData);
        $response->assertStatus(422);

    }

    public function testUpdateClassroomWithWrongStudentId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();


        $updateclassroomData = [
            "alunos" => [123456,245646],
        ];

        $response = $this->put('/api/turmas/'.$classroom->id,$updateclassroomData);
        $response->assertStatus(422);

    }

    public function testFindAllStudentsByClassRoomId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $classroom = Classroom::factory()->create();

        $response = $this->get('/api/turmas/'.$classroom->id.'/alunos');
        $response->assertStatus(200);
        $response->assertJsonCount($classroom->students->count());
    }

    public function testFindAllStudentsByClassRoomWithWrongId()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/api/turmas/250/alunos');
        $response->assertStatus(404);
    }

}
