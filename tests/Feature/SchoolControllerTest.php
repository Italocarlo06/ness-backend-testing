<?php

namespace Tests\Feature;

use App\Models\TeacherSchool;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SchoolControllerTest extends TestCase
{
   use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function testGetAllSchoolsByTeacher()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $teacherSchool = TeacherSchool::factory(5)->create([
            "user_id" => $user->id
        ]);

        $response = $this->get('/api/escolas');
        $response->assertStatus(200);
        $response->assertJsonCount($teacherSchool->count());

    }

    public function testGetAllSchoolsByTeacherWithNoSchool()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/api/escolas');
        $response->assertStatus(204);

    }
}
