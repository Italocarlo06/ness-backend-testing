<?php

namespace Database\Factories;

use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\pt_BR\Person;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Student>
 */
class StudentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        fake()->addProvider(new Person(fake()));

        $firstName = fake()->firstName();
        $lastName = fake()->lastName();

        $responsibleFirstName = fake()->firstName();
        $responsibleLastName = fake()->lastName();
        return [
            'name' => "{$firstName} {$lastName}",
            'responsible_name' => "{$responsibleFirstName} {$responsibleLastName}",
            'birth_date' => now(),
            'school_id' => School::factory(),
        ];
    }
}
