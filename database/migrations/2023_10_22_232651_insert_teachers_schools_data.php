<?php

use App\Models\User;
use App\Models\TeacherSchool;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $users = User::all();

        foreach ($users as $user) {
            TeacherSchool::create([
                'user_id' => $user->id,
                'school_id' => $user->school_id,
            ]);
        }

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        TeacherSchool::truncate();
    }
};
