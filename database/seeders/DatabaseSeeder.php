<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\School;
use App\Models\Student;
use App\Models\TeacherSchool;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(10)->create();
        School::factory(5)->create();

        $users = User::all();
        $schools = School::all();

        foreach ($users as $user){
            foreach ($schools as $school){
                TeacherSchool::create([
                    "user_id" => $user->id,
                    "school_id" => $school->id
                ]);

                Student::factory(5)->create([
                    "school_id" => $school->id
                ]);
            }
        }
    }
}
